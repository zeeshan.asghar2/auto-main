const { defineConfig } = require('cypress');

const createBundler = require('@bahmutov/cypress-esbuild-preprocessor');
const addCucumberPreprocessorPlugin = require('@badeball/cypress-cucumber-preprocessor').addCucumberPreprocessorPlugin;
const createEsbuildPlugin = require('@badeball/cypress-cucumber-preprocessor/esbuild').createEsbuildPlugin;
const { NodeGlobalsPolyfillPlugin } = require("@esbuild-plugins/node-globals-polyfill");

const { NodeModulesPolyfillPlugin } = require("@esbuild-plugins/node-modules-polyfill");

module.exports = defineConfig({
  testBaseUrl: 'https://test.singlerulebook.com/login?refer=/srb',
  defaultCommandTimeout: 30000,
  pageLoadTimeout: 30000,
  viewportHeight: 800,
  viewportWidth: 1280,
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    experimentalSessionAndOrigin: true,
    async setupNodeEvents(on, config) {
      const bundler = createBundler({
        plugins: [
          NodeModulesPolyfillPlugin(),
          NodeGlobalsPolyfillPlugin({
            process: true,
            buffer: true
          }),
          createEsbuildPlugin(config)
        ],
      });
      on("file:preprocessor", bundler);
      await addCucumberPreprocessorPlugin(on, config);
      on('task', {
        log(message) {
          console.log(message)
          return null;
        },
      });
      return config;
    },
    specPattern: 'cypress/e2e/**/*.feature',
    baseUrl: 'https://test.singlerulebook.com/login?refer=/srb',
    excludeSpecPattern: ['*.md'],
  },
});
