#!/bin/bash

# Start Xvfb on display :99 with a screen resolution of 1920x1080x24
Xvfb :99 -screen 0 1920x1080x24 -ac &
