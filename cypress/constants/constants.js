export default {
  srb_UserName: "qamar.riaz@kaizenreporting.com",
  srb_Password: "SingleRuleBook@12345",
  srb_InvalidUserName: "test.user@xyz.com",
  srb_InvalidPassword: "InvalidCredentials",
  userName: "test.user5425@kaizenreporting.com",
  userPassword: "1999@KaizenReporting",

  annotationColumnHeaderTitle: [
    "Regulation",
    "Item",
    "Annotation Notes",
    "Annotation Heading",
    "Attachments",
    "Last Updated By",
    "Private/Shared",
    "Last Modified",
    "Status Indicator"
  ],

  labelsColumnHeaderTitle: [
    "Regulation",
    "Item",
    "Selected Text",
    "Label",
    "Last Updated By",
    "Private/Shared",
    "Last Modified",
    "Status Indicator"
  ],
  UK_REGULATIONS: [
    "FCA",
    "Statutory Instruments",
    "MiFID II"
  ],

  US_REGULATIONS: [
    "CFTC",
    "Law"
  ],

  EU_REGULATIONS: [
    "AIFMD",
    "Accounting Directive",
    "Audit Directive",
    "BMR",
    "BRRD",
    "CCP RRR",
    "CRAR",
    "CRR / CRD",
    "CSDR",
    "Cross-border distribution of CIUs",
    "DORA",
    "EBA Regulation",
    "ECSPR / ECSPD",
    "ELTIF",
    "EMIR",
    "ESMA Regulation",
    "EuVECA / EuSEF",
    "GDPR",
    "IFR / IFD",
    "Listing Directive",
    "MAR",
    "MLD",
    "MMSR",
    "MiCA",
    "MiFID II / MiFIR",
    "NISD",
    "PR",
    "PRIIPs",
    "PSD2",
    "REMIT",
    "SFDR",
    "SFTR",
    "SRD",
    "SSR",
    "Securitisation Regulation",
    "TD",
    "UCITS",
    "WTR"
  ],

  EXCHANGES_REGULATIONS: [
    "Borsa Istanbul",
    "Borsa Italiana",
    "CME",
    "ECC",
    "EEX",
    "Eurex",
    "Euronext",
    "Euronext Clearing",
    "ICE Clear Europe",
    "ICE Endex",
    "ICE US",
    "ICE EU",
    "JSE",
    "KDPW",
    "LCH LTD",
    "LCH SA",
    "LME",
    "MEFF",
    "Nasdaq OMX Stockholm"
  ],

  PrivacyPolicySubElementsTitle: [
    "Who we are",
    "What kind of personal information do we collect",
    "Who may we disclose personal information to",
    "How do we obtain your consent",
    "Management of personal information",
    "How do we store personal information and for how long",
    "How do we use the personal information",
    "The personal information we hold about you",
    "What is a cookie and how do we use cookies",
    "Technology improvements",
    "Links to third party websites",
    "What if you have a complaint",
    "Contact Us"
  ],

  LegalMainMenuTitles: [
    "Kaizen Hub",
    "Terms and Conditions",
    "Privacy Policy",
    "Acceptable Use Policy",
    "Data Processing Addendum",
    "Support Policy"
  ],  

}
