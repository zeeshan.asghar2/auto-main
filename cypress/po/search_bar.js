/* global cy */

class SearchBar {

    // Element Selectors
    topSearchBarInput = "div.srb-topbar-search-form__textinput-wrapper > input"
    topSearchBarIcon = "button.srb-topbar-search-form__icon-button"
    searchResultsHeader = "div.srb-search-header"
    srbTopbarSearchDropdown = "a.srb-topbar-search-button"


    // User Actions
    inputValueInSearchBar(searchString){        
        cy.get(this.topSearchBarInput, { timeout: 30000 })
            .type(searchString)
    }

    clickSearchBarIcon(){        
        cy.get(this.topSearchBarIcon, { timeout: 30000 })
            .click({force: true})
    }

}

export default SearchBar;