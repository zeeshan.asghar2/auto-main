/* global cy */


class Jurisdictions {

    // Element Selectors
    ukJurisdictionMainLinkBtn = "button.srb-sidebar__header-button.srb-sidebar__header-button--uk5"
    regulationsTitle = "div.srb-sidebar-folder-container-top:nth-child(1) div:nth-child(1) span.srb-sidebar-folder-closed div.srb-sidebar-folder-element > div.srb-sidebar-folder-heading-root"
    regulationsCount = "div.srb-sidebar-folder-element > div.srb-sidebar-folder-heading-root"
    statutoryInstrumentsTitle = "div.srb-sidebar-folder-container-top:nth-child(2) div:nth-child(1) span.srb-sidebar-folder-closed div.srb-sidebar-folder-element > div.srb-sidebar-folder-heading-root"
    mifidTitle = "div.srb-sidebar-folder-container-top:nth-child(3) div:nth-child(1) span.srb-sidebar-folder-closed div.srb-sidebar-folder-element > div.srb-sidebar-folder-heading-root"
    usJurisdictionMainLinkBtn = "button.srb-sidebar__header-button.srb-sidebar__header-button--us6"
    euJurisdictionMainLinkBtn = "button.srb-sidebar__header-button.srb-sidebar__header-button--eu4"
    exchangesJurisdictionMainLinkBtn = "button.srb-sidebar__header-button.srb-sidebar__header-button--exchanges7"  
    
    
    // User Actions

    navigateToEachRegulation(indexValue){
        let finalIndex = indexValue + 1;
        this.regulationsTitle = `div.srb-sidebar-folder-container-top:nth-child(${finalIndex}) div:nth-child(1) span.srb-sidebar-folder-closed div.srb-sidebar-folder-element > div.srb-sidebar-folder-heading-root`
        return this.regulationsTitle
    }
}

export default Jurisdictions;