/* global cy */

class NavBarUserProfile {

    // Element Selectors
    profileIcon = "div[class='navbar-profile-icon-circle']"
    manageProfile = " div.float-right div.dropdown.show div.dropdown-menu.show > button.top-nav-profile-dropdown-item.dropdown-item:nth-child(1)"
    signOutBtn = "div.dropdown.show div.dropdown-menu.show > button.top-nav-profile-dropdown-item.dropdown-item:nth-child(4)"
    manageProfileHeader = "h3[class='text-center profile__h3 margin-top-35']"
    manageProfleFirstName = "input[placeholder='First name']"
    emptyNameFieldErrorMessage = "div[class='login-email-error-no-padding']"
    userProfileUpdateSuccess = "div[class='profile-success__text']"
    userPRofileUpdateSuccessCloseBtn = "div[class='profile-button-hollow']"     
    manageProfileLastName = "input[placeholder='Last name']"
    manageProfileEmail = "input[placeholder='Last name']"
    manageProfileChangePasswordLink = "div[class='forgot-password-link-style']"
    manageProfileMFALink = "div[class='forgot-password-link-style margin-bottom-12']"
    changePasswordPageHeader = "div[class='text-center profile__h3']"
    enterYourPassword = "div[class='login-title-style']"
    currentPassword = "input[placeholder='Current Password']"
    newPassword = "input[data-testid='password-input']"
    confirmPassword = "input[data-testid='confirm-password-input']"
    updateBtn = "button[class='common-primary-button common-primary-button']"
    saveBtn = "button[class='common-primary-button width-125']"
    cencelBtn = "button[class='profile-button-hollow']"
    passwordInputError = "div[data-testid='password-input-error']"
    confirmPasswordInputError = "div[data-testid='confirm-password-input-error']"
    multiFactorHeader = "h3[class='text-center profile__h3']"

    
    // User Actions

    clickUserProfileIconBtn(){
        cy.get(this.profileIcon, { timeout: 30000 })
            .click({force: true}) 
    }

    clickManageProfileChangePasswordLink(){
        cy.get(this.manageProfileChangePasswordLink, { timeout: 30000 })
            .click({force: true}) 
    }

    navigateManageProfilePage(){           
            cy.get(this.manageProfile, { timeout: 30000 })
            .click({force: true})
    }

    updateUserProfileValidCase() {
        cy.get(this.manageProfleFirstName, { timeout: 30000 })
            .clear()
            .type("Qamar")
    }

    clickUserProfileSaveButton(){
        cy.get(this.saveBtn, { timeout: 30000 })
            .click({force: true})
    }

    clickMultiFactorLinkButton(){
        cy.get(this.manageProfileMFALink, { timeout: 30000 })
            .click({force: true})
    }

    updateUserProfileInValidCase() {
        cy.get(this.manageProfleFirstName, { timeout: 30000 })
            .clear()
    }

    inputNewPassword(newPassword) {
        cy.get(this.newPassword)
          .clear()
          .type(newPassword, { delay: 100})
    }

    inputConfirmPassword(confirmPassword) {
        cy.get(this.confirmPassword)
          .clear()
          .type(confirmPassword, { delay: 100})
    }

    changePassword(currentPassword, newPassword, confirmPassword) {
        cy.get(this.currentPassword)
            .clear()
            .type(currentPassword)

        cy.get(this.newPassword)
          .clear()
          .type(newPassword, { delay: 100})

        cy.get(this.confirmPassword)
          .clear()
          .type(confirmPassword)
          .wait(500)               
      }

}

export default NavBarUserProfile;