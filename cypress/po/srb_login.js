/* global cy */

class SRBLoginPage {

    // Element Selectors
    loginPageTitle = "div.login-welcome-class"
    userNameField = ".common-email-input "
    passwordField = "input.login-input-style"
    loginButton = "button.common-submit-button"
    SRBHomePage = "h3.srb-pagelayout__title"
    errorMessage ='.common-password-error > .float-left';
    profileIcon = 'div[class="navbar-profile-icon-text"]';
    signOutBtn ='button.top-nav-profile-dropdown-item:nth-child(3)';

      // User Actions
      inputCredentials(userName, password) {
        cy.get(this.userNameField)
          .type(userName)
          .get(this.passwordField)
          .type(password)
          
      }

      clickLoginBtn()
      {
        cy.get(this.loginButton)
        .click({ force: true })
      }

      logout(){
        cy.get(this.profileIcon)
        .click({ force: true })
        .get(this.signOutBtn)
        .click({ force: true })
      }

}
export default SRBLoginPage;