/* global cy */

class Annotations {

    // Element Selectors
    navigateToArticle = "srb/regs/eu/aifmd/article-18-of-aifmd"
    articleTitleHeader = "p[id='id-5e5e42a8-f7dd-44ee-9888-91da5c479040']"
    annotationIcon = "img[alt='srb-annotation-icon']"
    createNewBtn = "button[class='srb-annotationpanel__create-button']"
    cancelBtn = '.btn-light'
    headingSelection = '.css-1wa3eu0-placeholder'
    annotationDescription = "div[class='public-DraftStyleDefault-block public-DraftStyleDefault-ltr']"
    saveBtn = "button[class='btn btn-burnt-orange']"

    toastMessage = ".srb-toast-content > span"

    purpleAnnotationIcon = "div.srb-message-annotation-marker-shared";
    annotationPanelTitle = "p.srb-annotationpanel__title";
    annotationDescriptionText = '.srb-annotationpanel__annotation-read-content-box';
    deleteBtn = "button.srb-richeditor-bin-button";


    // User Actions
    userNavigateToArticle() {
        cy.visit(this.navigateToArticle)
    }

    clickAnnotationIcon() {
        cy.get(this.annotationIcon, { timeout: 30000 })
            .click({ force: true })
    }

    clickCreateNewBtn() {
        cy.get(this.createNewBtn, { timeout: 30000 })
            .click({ force: true })
    }

    createAnnotationHeading() {
        cy.get(this.headingSelection, { timeout: 30000 })
            .type('TestAutomation_DoNotDelete{enter}')
    }

    inputAnnotationDescription() {
        cy.get(this.annotationDescription, { timeout: 30000 })
            .type('Test Annotation Description')
    }

    clickSaveButton() {
        cy.get(this.saveBtn, { timeout: 30000 })
            .click({ force: true })
    }

    clickPurpleAnnotationIconButton() {
        cy.get(this.purpleAnnotationIcon, { timeout: 30000 })
            .click({ force: true })
    }

    clickAnnotationDescriptionBox() {
        cy.get(this.annotationDescriptionText, { timeout: 30000 })
            .click({ force: true })
    }

    updateAnnotationDescription() {
        cy.get(this.annotationDescription, { timeout: 30000 })
            .clear()
                .type('Test Annotation Description Updated again')
    }

    clickDeleteButton() {
        cy.get(this.deleteBtn, { timeout: 30000 })
            .click({ force: true })
    }
}

export default Annotations;