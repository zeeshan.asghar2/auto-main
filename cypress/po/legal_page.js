/* global cy */

class LegalPage {

    // Element Selectors

    legal = "div.dropdown.show div.dropdown-menu.show > button.top-nav-profile-dropdown-item.dropdown-item:nth-child(3)"
    legalPageHeader = "h1[class='legal-title']"
    legalMainMenu = "li > div.legal-sidenav-option"
    legalSubMenu = 'div.legal-sidenav-second-tier > li.legal-sidenav-list-item.legal-sidenav-list-item--not-selected:nth-child(1) > div.legal-sidenav-option.legal-sidenav-option--colorB'
    legalSubMenuCount = 'div.legal-sidenav-second-tier > li.legal-sidenav-list-item.legal-sidenav-list-item--not-selected > div.legal-sidenav-option.legal-sidenav-option--colorB'
    privacyPolicyMainMenu = "div.legal-sidenav-list-item:nth-child(3) > li.legal-sidenav-list-header"


    // User Actions
    navigateLegalsPage(){        
        cy.get(this.legal, { timeout: 30000 })
            .click({force: true})
    }

    clickPrivacyPolcyMainMenu(){
        cy.get(this.privacyPolicyMainMenu, { timeout: 30000 })
        .click({force: true})
    }

    navigateToEachSubMenuOfPrivacyPolicy(indexValue){
        let finalIndex = indexValue + 1;
        this.legalSubMenu = `div.legal-sidenav-second-tier > li.legal-sidenav-list-item.legal-sidenav-list-item--not-selected:nth-child(${finalIndex}) > div.legal-sidenav-option.legal-sidenav-option--colorB`
        return this.legalSubMenu
    }


}

export default LegalPage;