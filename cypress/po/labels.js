/* global cy */

class Labels {

    // Element Selectors
    labelIcon = ".srb-annotationpanel__tab > img[alt='srb-integration-icon']"
    labelCreateNewBtn = "button.srb-annotationpanel__create-button"
    labelHeadingSelection = '.css-1wa3eu0-placeholder'
    labelHeadingCrossIconBTn = "div.css-1wy0on6 > div:nth-child(1) > svg"
    labelSelectText = "div[class='d-flex align-items-center'] > button"
    createNewLabelDialogHeader = "h2[class='srb-modal__title']"
    labelParagraph = "div.srb-modal__content div.srb-integrations-modal__text-selection span.srb-documentviewer-inner-html-span.false div.eurlex div.eurlex-norm.eurlexsolo:nth-child(3) div.eurlex-norm.eurlex-inline-element.eurlexsolo > span:nth-child(1)"
    labelConfirmSelection = "button[class='btn btn-burnt-orange']"
    labelRemoveBtn = "button[class='btn btn-link btn-link--danger btn-sm']"
    labelSaveBtn = "button[class='btn btn-burnt-orange btn-sm']"
    labelToastMessage = ".srb-toast-content > span"
    labelThreeDotMenu = "svg[class='MuiSvgIcon-root']"
    labelEditBtn = "div.srb-integrationpanel__more-dd-menu.dropdown-menu.show > button:nth-child(1)"
    labelDeleteBtn = "div.srb-integrationpanel__more-dd-menu.dropdown-menu.show > button:nth-child(2)"


    // User Actions

    clickLabelIconMethod(){
        cy.get(this.labelIcon, { timeout: 30000 })
            .click({ force: true })
    }

    clickCreateNewButton(){
        cy.get(this.labelCreateNewBtn, { timeout: 30000 })
            .click({ force: true })
    }

    createLabelHeading() {
        cy.get(this.labelHeadingSelection, { timeout: 30000 })
            .type('TestAutomationLabel_DoNotDelete{enter}')
    }

    updateLabelHeading() {
        cy.get(this.labelHeadingCrossIconBTn, { timeout: 30000 }).click({ force: true })
        
        cy.get(this.labelHeadingSelection, { timeout: 30000 })
            .type('Updated_TestAutomationLabel_DoNotDelete{enter}')
    }

    clickSelectTextLinkButton(){
        cy.get(this.labelSelectText, { timeout: 30000 })
            .click({ force: true })
    }

    selectTextFromParagraph(){
        cy.get(this.labelParagraph, { timeout: 30000 }).contains('adequate and appropriate human and technical resources').click().type('{ctrl+a}')
    }

    clickConfirmSelectionButton(){
        cy.get(this.labelConfirmSelection, { timeout: 30000 })
            .click({ force: true })
    }

    clickSaveButton(){
        cy.get(this.labelSaveBtn, { timeout: 30000 })
            .click({ force: true })
    }

    clickThreeDotMenuButton(){
        cy.get(this.labelThreeDotMenu, { timeout: 30000 })
            .click({ force: true })
    }

    clickThreeDotEditButton(){
        cy.get(this.labelEditBtn, { timeout: 30000 })
            .click({ force: true })
    }

    clickThreeDotDeleteButton(){
        cy.get(this.labelDeleteBtn, { timeout: 30000 })
            .click({ force: true })
    }

}

export default Labels;