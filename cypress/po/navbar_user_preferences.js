/* global cy */

class UserPreferences {

    // Element Selectors

    userPreferences = "div.dropdown.show div.dropdown-menu.show > button.top-nav-profile-dropdown-item.dropdown-item:nth-child(2)"
    userPreferencesHeader = "h3[class='srb-pagelayout__title']"
    jurisdictionColumnHeader = "div.srb-user-pref div:nth-child(1) table.srb-table thead:nth-child(1) tr:nth-child(2) > th:nth-child(2)"
    regulationColumnHeader = "div.srb-user-pref div:nth-child(1) table.srb-table thead:nth-child(1) tr:nth-child(2) > th:nth-child(3)"
    regulationAlertsColumnHeader = "div.srb-user-pref div:nth-child(1) table.srb-table thead:nth-child(1) tr:nth-child(2) > th:nth-child(4)"
    annotationAlertsColumnHeader = "div.srb-user-pref div:nth-child(1) table.srb-table thead:nth-child(1) tr:nth-child(2) > th:nth-child(5)"
    labelsAlertsColumnHeader = "div.srb-user-pref div:nth-child(1) table.srb-table thead:nth-child(1) tr:nth-child(2) > th:nth-child(6)"
    privateAnnotHeader = "div.srb-user-pref div:nth-child(1) table.srb-table thead:nth-child(1) tr:nth-child(3) > th.text-center:nth-child(1)"
    sharedAnnotHeader = "div.srb-user-pref div:nth-child(1) table.srb-table thead:nth-child(1) tr:nth-child(3) > th.text-center:nth-child(2)"

    jurisdictionEU = "div.srb-user-pref__jurisdictions.justify-content-start > div > div > label > div"
    jurisdictionUK = "input[name='userPref-uk']"
    jurisdictionUS = "input[name='userPref-us']"
    jurisdictionExchanges = "input[name='userPref-exchanges']"

    infoBtn = "button[class='btn btn-link p-1']"
    userPreferencesModalHeader = "h2[class='srb-modal__title'] > span"
    closeBtn = "button[class='btn btn-burnt-orange']"

    // User Actions
    navigateUserPreferencesPage(){        
        cy.get(this.userPreferences, { timeout: 30000 })
            .click({force: true})
    }

    navigateUserPreferencesInformationDialog(){        
        cy.get(this.infoBtn, { timeout: 30000 })
            .first()
                .click({force: true})
    }

    clickCloseButton(){        
        cy.get(this.closeBtn, { timeout: 30000 })
            .last()
                .click({force: true})
    }
}

export default UserPreferences;