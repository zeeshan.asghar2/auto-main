/* global cy */

class SRBHomePage {

    // Element Selectors
    SRBHomePage = "h3.srb-pagelayout__title"
    annotationSectionTitle = "div.srb-home__top-row-box:nth-child(1) > p.srb-home__title"
    labelSectionTitle = "div.srb-home__top-row-box:nth-child(2) > p.srb-home__title"
    updatesSectionTitle = "div.srb-home__update-feeds > div > div.d-flex.justify-content-between.align-items-baseline > p"
    userInformationSectionTitle = "div.d-flex.flex-column.flex-grow-1 div.srb-home__bottom-row > p.srb-home__title"
    userGuideSectionTitle = "div.srb-home__bottom-row-box:nth-child(1) > p.srb-home__bottom-row-box-title:nth-child(2)"
    contactSupportSectionTitle = "div.srb-home__bottom-row-box:nth-child(2) > p.srb-home__bottom-row-box-title:nth-child(2)"
    seeMoreBtnAnnotationSummary = "div.srb-home__top-row-box:nth-child(1) > button.btn.btn-link.p-0"
    seeMoreBtnLabelSummary = "div.srb-home__top-row-box:nth-child(2) > button.btn.btn-link.p-0"
    pageMainHeading = "h3.srb-pagelayout__title"
    annotationColumnHeaderName = ".common-drag-and-drop-column__draggable:nth-child(2) > div > span"
    columnHeaderCount = ".common-drag-and-drop-column__draggable"
    regulationChangeUpdateBtn = "div.srb-home__update-feeds div.srb-update-feeds div.srb-update-feeds__content div.infinite-scroll-component__outerdiv div.infinite-scroll-component ul.srb-update-feeds__list.list-unstyled button.srb-update-feeds__button:nth-child(1) li.srb-update-feeds__item div.srb-update-feeds__details > div.srb-update-feeds__details-subtitle"
    updatePageFirstArticleTitle = "ul.list-unstyled.srb-updates__list li.srb-updates__list-item.srb-updates__list-item--reg-change:nth-child(1) div.d-flex div.srb-updates__item-details > div.d-flex.mb-4:nth-child(1)"
    filterOptionSection = "h4.srb-updates__filters-title"
    userManualLinkBtn = "div.srb-home__bottom-row-box:nth-child(1) > a.srb-home__bottom-row-box-link:nth-child(3)"
    videoGuidesLinkBtn = "div.srb-home__bottom-row-box:nth-child(1) > button.btn.btn-link.srb-home__bottom-row-box-button:nth-child(4)"
    videoGuidesDialogTitle = "div.srb-video-guide-modal__thumbnails-title"
    
    
    // User Actions

    navigateToEachColumnHeader(indexValue){
        let finalIndex = indexValue + 1;
        this.annotationColumnHeaderName = `.common-drag-and-drop-column__draggable:nth-child(${finalIndex}) > div > span`
        return this.annotationColumnHeaderName
    }
}
export default SRBHomePage;