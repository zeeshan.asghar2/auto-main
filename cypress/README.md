# Chosen tool for End to End Testing

Cypress is the most advanced tool for E2E testing and is dev friendly. More information about [Cypress](https://www.cypress.io).

## More info

- Tests are written in simple mocha style with `describe` and `it` statements. 
- Abstraction is implemented to reflect Page Object Design Pattern. 
- Commonly used actions are in `utils` file. 
- We are not supposed to declare or write any DOM element selectors in the test file. It has to be in the respective pages.

## How to run it

- We are running the tests against the `inlcuded docker image` which has all the cypress dependency installed.
- Running headless tests: `npm run cy:run`
- Running tests Interactively : `npm run cy:open` (Note: This might not work if you don't have the setup required to forward X11. Please consult a team member to compelte the setup.)


## Notes
- For now the Cypress tests are running against the dev branch. We should instead first spin the local server and test against the local branch.