/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import utils from '../../utils/utils';
import Annotations from '../../po/annotations';

let obj_annotation = new Annotations();

Given('User navigates to the article', () => {
    obj_annotation.userNavigateToArticle()
});

Given('Respective article should be loaded successfully', () => {
    utils.verifyIsVisible(obj_annotation.articleTitleHeader)
});

Given('User clicks annotation icon', () => {
    obj_annotation.clickAnnotationIcon()
});

Given('User clicks create new button', () => {
    obj_annotation.clickCreateNewBtn()
});

Given('User select or create a new heading of the annotation', () => {
    obj_annotation.createAnnotationHeading()
});

Given('User input description', () => {
    obj_annotation.inputAnnotationDescription()
});

When('User navigates to any article', () => {
    obj_annotation.userNavigateToArticle()
});

Then('Article should be loaded successfully', () => {
    utils.verifyIsVisible(obj_annotation.articleTitleHeader)
});

Then('Annotation icon should show', () => {
    utils.verifyIsVisible(obj_annotation.annotationIcon)
});

When('User click save button', () => {
    obj_annotation.clickSaveButton()
});

Then('Annotation should be added successfully', () => {
    utils.verifyIsVisible(obj_annotation.toastMessage)
});

Then('Annotation created success message should show', () => {
    utils.containsText(obj_annotation.toastMessage, "Success: Annotation created")
});

Given('User clicks purple annotation icon', () => {
    obj_annotation.clickPurpleAnnotationIconButton()
});

Given('Annotation heading populated', () => {
    utils.containsText(obj_annotation.annotationPanelTitle, "TestAutomation_DoNotDelete")
});

Given('User clicks the description box of the created annotation', () => {
    obj_annotation.clickAnnotationDescriptionBox()
});

Given('User updates any data of the annotation', () => {
    obj_annotation.updateAnnotationDescription()
});

Then('Annotation should be updated successfully', () => {
    utils.verifyIsVisible(obj_annotation.toastMessage)
});

Then('Annotation updated success message should show', () => {
    utils.containsText(obj_annotation.toastMessage, "Success: Annotation updated")
});

When('User clicks on delete button icon', () => {
    obj_annotation.clickDeleteButton()
});

Then('Annotation should be deleted successfully', () => {
    utils.verifyIsVisible(obj_annotation.toastMessage)
});

Then('Annotation deleted success message should show', () => {
    utils.containsText(obj_annotation.toastMessage, "Success: Annotation deleted")
});

Then('Annotation should not be added', () => {
    utils.verifyIsVisible(obj_annotation.toastMessage)
});

Then('Error message should show for missing annotation heading', () => {
    utils.containsText(obj_annotation.toastMessage, "Error: Please enter an annotation heading")
});