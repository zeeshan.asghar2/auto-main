/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import utils from '../../utils/utils';
import Labels from '../../po/labels';

let obj_labels = new Labels();

Given('User clicks label icon', () => {
    obj_labels.clickLabelIconMethod()
});

Given('User clicks create new button of label', () => {
    obj_labels.clickCreateNewButton()
});

Given('User create a new heading of the label', () => {
    obj_labels.createLabelHeading()
});

Given('User clicks select text link button', () => {
    utils.containsText(obj_labels.labelSelectText, 'Select text...')
    obj_labels.clickSelectTextLinkButton()
});

Given('User selects text', () => {
    utils.containsText(obj_labels.createNewLabelDialogHeader, 'Create New Label')
    obj_labels.selectTextFromParagraph()
});

Given('User clicks confirm selection button', () => {
    obj_labels.clickConfirmSelectionButton()
});

Given('User clicks on the three dot menu of the created label', () => {
    obj_labels.clickThreeDotMenuButton()
});

Given('User clicks edit button', () => {
    obj_labels.clickThreeDotEditButton()
});

Given('User updates any data of the labels', () => {
    obj_labels.updateLabelHeading()
});

When('User clicks delete button', () => {
    obj_labels.clickThreeDotDeleteButton()
});

When('User click save button of label section', () => {
    obj_labels.clickSaveButton()
});

Then('Labels icon should show', () => {
    utils.verifyIsVisible(obj_labels.labelIcon)
});

Then('Label should be added successfully', () => {
    utils.verifyIsVisible(obj_labels.labelToastMessage)
});

Then('Label created success message should show', () => {
    utils.containsText(obj_labels.labelToastMessage, 'Success: Label created')
});

Then('Label should be updated successfully', () => {
    utils.verifyIsVisible(obj_labels.labelToastMessage)
});

Then('Label updated success message should show', () => {
    utils.containsText(obj_labels.labelToastMessage, 'Success: Label updated')
});


Then('Label should be deleted successfully', () => {
    utils.verifyIsVisible(obj_labels.labelToastMessage)
});

Then('Label deleted success message should show', () => {
    utils.containsText(obj_labels.labelToastMessage, 'Success: Label deleted')
});

Then('Label should not be added', () => {
    utils.verifyIsVisible(obj_labels.labelToastMessage)
});

Then('Label created failed message should show', () => {
    utils.containsText(obj_labels.labelToastMessage, 'Error: Label heading is required')
});