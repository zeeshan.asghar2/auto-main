/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import utils from '../../utils/utils';
import NavBarUserProfile from '../../po/navbar_user_profile';
import UserPreferences from '../../po/navbar_user_preferences';

let obj_userProfile = new NavBarUserProfile()
let obj_userPreferences = new UserPreferences();

Given('User navigates to the user preferences', () => {
    obj_userProfile.clickUserProfileIconBtn()
    obj_userPreferences.navigateUserPreferencesPage()
});

Given('User preferences page should open', () => {
    utils.verifyIsVisible(obj_userPreferences.userPreferencesHeader)
});


When('User verifies data table of user preferences', () => {
    cy.log("User verifies column header for user preferences page")
});

Then('User preferences page should shows all the columns', () => {
    utils.verifyIsVisible(obj_userPreferences.jurisdictionColumnHeader)
    utils.verifyIsVisible(obj_userPreferences.regulationColumnHeader)
    utils.verifyIsVisible(obj_userPreferences.regulationAlertsColumnHeader)
    utils.verifyIsVisible(obj_userPreferences.annotationAlertsColumnHeader)
    utils.verifyIsVisible(obj_userPreferences.labelsAlertsColumnHeader)
    utils.verifyIsVisible(obj_userPreferences.privateAnnotHeader)
    utils.verifyIsVisible(obj_userPreferences.sharedAnnotHeader)
});

Then('User preferences columns name should be correct', () => {
    utils.containsText(obj_userPreferences.jurisdictionColumnHeader, "Jurisdiction")
    utils.containsText(obj_userPreferences.regulationColumnHeader, "Regulation")
    utils.containsText(obj_userPreferences.regulationAlertsColumnHeader, "Receive all alerts for a regulation")
    utils.containsText(obj_userPreferences.annotationAlertsColumnHeader, "Receive alert for Annotations")
    utils.containsText(obj_userPreferences.labelsAlertsColumnHeader, "Receive alert for Labels (Always shared)")
    utils.containsText(obj_userPreferences.privateAnnotHeader, "Private")
    utils.containsText(obj_userPreferences.sharedAnnotHeader, "Shared")
});

When('User verifies that checkboxes are showing for jurisdictions', () => {
    cy.log("User verifies column header for user preferences page")
});

Then('Four checkboxes should be visible one for each jurisdiction', () => {
    utils.verifyIsVisible(obj_userPreferences.jurisdictionColumnHeader)
    utils.verifyIsVisible(obj_userPreferences.regulationColumnHeader)
    utils.verifyIsVisible(obj_userPreferences.regulationAlertsColumnHeader)
    utils.verifyIsVisible(obj_userPreferences.annotationAlertsColumnHeader)
});

When('User verifies the jurisdictions are showing on user preferences page', () => {
    cy.log("User verifies the jurisdictions are showing on user preferences page")
});

Then('All four jurisdictions should be visible on user preferences page', () => {
    utils.containsText(obj_userPreferences.jurisdictionEU, "EU")
    utils.containsText(obj_userPreferences.jurisdictionEU, "UK")
    utils.containsText(obj_userPreferences.jurisdictionEU, "US")
    utils.containsText(obj_userPreferences.jurisdictionEU, "EXCHANGES")
    cy.get(obj_userPreferences.infoBtn).first().click({ force: true })
});


When('User clicks info icon button', () => {
    obj_userPreferences.navigateUserPreferencesInformationDialog()
});

Then('User preferences information dialog screen should open', () => {
    utils.containsText(obj_userPreferences.userPreferencesModalHeader, "User Preferences")
});


Then('User should be able to see close button', () => {
    utils.verifyIsVisible(obj_userPreferences.closeBtn)
});

Then('User should be able to click close button', () => {
    obj_userPreferences.clickCloseButton()
});