/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import utils from '../../utils/utils';
import constants from '../../constants/constants';
import Jurisdictions from '../../po/jurisdictions';
import SRBHomePage from '../../po/srb_homePage';

let obj_jurisdiction = new Jurisdictions()
let obj_home = new SRBHomePage()

// Given('User is on home page of srb', ()=> {
//     cy.loginSRB(constants.srb_UserName, constants.srb_Password)
// });

Given('home page loaded successfully', ()=> {
    utils.verifyIsVisible(obj_home.seeMoreBtnAnnotationSummary)
});

When('User clicks on UK jurisdiction', () => {    
    utils.clickOnElement(obj_jurisdiction.ukJurisdictionMainLinkBtn)
});

Then('All the regulations associated with UK jurisdiction should show correctly', () => {

    let count = constants.UK_REGULATIONS.length
    let regulationTitles = constants.UK_REGULATIONS
    utils.verifyElementsLength(obj_jurisdiction.regulationsCount, '3')

    for(let i = 0; i < count; i+=1) {
        let element = obj_jurisdiction.navigateToEachRegulation(i)
        utils.containsText(element, regulationTitles[i])
        }
});

When('User clicks on US jurisdiction', () => {
    utils.clickOnElement(obj_jurisdiction.usJurisdictionMainLinkBtn)
});

Then('All the regulations associated with US jurisdiction should show correctly', () => {

    let count = constants.US_REGULATIONS.length
    let regulationTitles = constants.US_REGULATIONS
    utils.verifyElementsLength(obj_jurisdiction.regulationsCount, '2')

    for(let i = 0; i < count; i+=1) {
        let element = obj_jurisdiction.navigateToEachRegulation(i)
        utils.containsText(element, regulationTitles[i])
        }
});

When('User clicks on EU jurisdiction', () => {
    utils.clickOnElement(obj_jurisdiction.euJurisdictionMainLinkBtn)
});

Then('All the regulations associated with EU jurisdiction should show correctly', () => {

    let count = constants.EU_REGULATIONS.length
    let regulationTitles = constants.EU_REGULATIONS
    utils.verifyElementsLength(obj_jurisdiction.regulationsCount, '38')

    for(let i = 0; i < count; i+=1) {
        let element = obj_jurisdiction.navigateToEachRegulation(i)
        utils.containsText(element, regulationTitles[i])
        }
});

When('User clicks on Exchanges jurisdiction', () => {
    utils.clickOnElement(obj_jurisdiction.exchangesJurisdictionMainLinkBtn)
});

Then('All the regulations associated with Exchanges jurisdiction should show correctly', () => {

    let count = constants.EXCHANGES_REGULATIONS.length
    let regulationTitles = constants.EXCHANGES_REGULATIONS
    utils.verifyElementsLength(obj_jurisdiction.regulationsCount, '19')

    for(let i = 0; i < count; i+=1) {
        let element = obj_jurisdiction.navigateToEachRegulation(i)
        utils.containsText(element, regulationTitles[i])
        }
});