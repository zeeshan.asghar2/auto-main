/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import utils from '../../utils/utils';
import SearchBar from '../../po/search_bar';

let obj_search = new SearchBar()

Given('User input {string} in search bar', (searchString)=> {
    obj_search.inputValueInSearchBar(searchString)
});

When('User clicks search bar icon', () => {
    obj_search.clickSearchBarIcon()
});

Then('User should be able to see the retrieved results', ()=> {
    utils.containsText(obj_search.searchResultsHeader, `Search results for "mtf"562 results`)
});

Then('User should not be able to see the any results', ()=> {
    utils.containsText(obj_search.searchResultsHeader, `Search results for "mtf123"0 results`)
});