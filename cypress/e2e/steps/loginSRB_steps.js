/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import SRBLoginPage from '../../po/srb_login';
import utils from '../../utils/utils';
import constants from '../../constants/constants';

let login = new SRBLoginPage()

Given('User is on Login page of SRB application', () => {
    cy.visit(Cypress.config("baseUrl"))
    utils.verifyIsVisible(login.loginPageTitle)
});

Given('User input valid credentials', ()=> {
    login.inputCredentials(constants.srb_UserName, constants.srb_Password)
});

Given('User input invalid credentials', ()=> {
    login.inputCredentials(constants.srb_InvalidUserName, constants.srb_InvalidPassword)
});

When('User clicks the login button', () => {
    login.clickLoginBtn()
});

Then('User should be able to login successfully and home dashboard page should show', () => {
    utils.verifyIsVisible(login.SRBHomePage)
});

Then('Error message should show', () => {
    utils.verifyIsVisible(login.errorMessage)
    utils.verifyText(login.errorMessage, "These details are incorrect.")
});

