/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import utils from '../../utils/utils';
import constants from '../../constants/constants';
import SRBHomePage from '../../po/srb_homePage';


let homePage = new SRBHomePage()

Given('User is on home page of srb', ()=> {
    cy.loginSRB(constants.srb_UserName, constants.srb_Password)
});

When('User verifies home page sections', () => {
    utils.verifyIsVisible(homePage.annotationSectionTitle)
});

Then('User should be able to see all sections of home page', () => {
    utils.verifyIsVisible(homePage.labelSectionTitle)
    utils.verifyIsVisible(homePage.updatesSectionTitle)
    utils.verifyIsVisible(homePage.userInformationSectionTitle)
    utils.verifyIsVisible(homePage.userGuideSectionTitle)
    utils.verifyIsVisible(homePage.contactSupportSectionTitle)
});

When('User clicks on see more button', () => {
    cy.get(homePage.seeMoreBtnAnnotationSummary).click({force:true})
});

Then('User should be navigated to annotations summary page', () => {
    utils.verifyIsVisible(homePage.pageMainHeading)
    utils.containsText(homePage.pageMainHeading, "Annotations")
});

Then('All the column headers names of annotations summary showing properly', () => {

    let count = constants.annotationColumnHeaderTitle.length
    let columnTitle = constants.annotationColumnHeaderTitle
    utils.verifyElementsLength(homePage.columnHeaderCount, '9')

    for(let i = 0; i < count; i+=1) {
        let element = homePage.navigateToEachColumnHeader(i)
        utils.containsText(element, columnTitle[i])
        }
});


When('User clicks on see more button of labels section', () => {
    cy.get(homePage.seeMoreBtnLabelSummary).click({force:true})
});

Then('User should be navigated to labels summary page', () => {
    utils.verifyIsVisible(homePage.pageMainHeading)
    utils.containsText(homePage.pageMainHeading, "Labels")
});

Then('All the column headers names of labels summary showing properly', () => {

    let count = constants.labelsColumnHeaderTitle.length
    let columnTitle = constants.labelsColumnHeaderTitle
    utils.verifyElementsLength(homePage.columnHeaderCount, '8')

    for(let i = 0; i < count; i+=1) {
        let element = homePage.navigateToEachColumnHeader(i)
        utils.containsText(element, columnTitle[i])
        }
});

When('User clicks any notification message', () => {
    cy.get(homePage.regulationChangeUpdateBtn).click({multiple: true})
});

Then('Updates main page should open', () => {
    utils.verifyIsVisible(homePage.pageMainHeading)
    utils.containsText(homePage.pageMainHeading, "Updates")
});

Then('Clicked notification should be showing at the top successfully', () => {
    utils.verifyIsVisible(homePage.updatePageFirstArticleTitle)
});

Then('Filter options should be showing properly on Updates main page', () => {
    utils.verifyIsVisible(homePage.filterOptionSection)
    utils.containsText(homePage.filterOptionSection, "Filter Options")
});

Given('User scrolls down to the bottom of the home page', ()=> {
    cy.get(homePage.userGuideSectionTitle).scrollIntoView()
});

When('User clicks on User Manual', () => {
    //cy.get(homePage.userManualLinkBtn).invoke('removeAttr', 'target').click()
    cy.get(homePage.userManualLinkBtn).click()
});

Then('Single rulebook user guide should open', () => {
    cy.log("User manual opened successfully")
    //cy.url().should('include', '/srb/images/9684b7d6ecfd7f022d0b31fbe7d6c494')
});

When('User clicks on video guides option', () => {
    cy.get(homePage.videoGuidesLinkBtn).click()
});

Then('Video guides dialog should open', () => {
    utils.verifyIsVisible(homePage.videoGuidesDialogTitle)
    utils.containsText(homePage.videoGuidesDialogTitle, "Other related videos")
        
});

