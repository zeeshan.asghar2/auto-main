/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import utils from '../../utils/utils';
import NavBarUserProfile from '../../po/navbar_user_profile';
import LegalPage from '../../po/legal_page';
import constants from '../../constants/constants';

let obj_userProfile = new NavBarUserProfile()
let obj_lagelPage = new LegalPage();

Given('User navigates to the legal page', () => {
    obj_userProfile.clickUserProfileIconBtn()
    obj_lagelPage.navigateLegalsPage()
});

Given('Legal page should open', () => {
    utils.verifyIsVisible(obj_lagelPage.legalPageHeader)
});


When('User verifies left bar main menu', () => {
    cy.log("User verifies left bar main menu")
});

Then('Legal main menus should be showing', () => {

    let count = constants.LegalMainMenuTitles.length
    let mainMenuTitles = constants.LegalMainMenuTitles
    utils.verifyElementsLength(obj_lagelPage.legalMainMenu, '6')

    for(let i = 0; i < count; i+=1) {
        let element = obj_lagelPage.legalMainMenu
        utils.containsText(element, mainMenuTitles[i])
        }
});

When('User clicks on privacy policy', () => {
    obj_lagelPage.clickPrivacyPolcyMainMenu()
});

Then('All the sub menus of privacy policy should show', () => {

    let count = constants.PrivacyPolicySubElementsTitle.length
    let subMenuTitles = constants.PrivacyPolicySubElementsTitle
    utils.verifyElementsLength(obj_lagelPage.legalSubMenuCount, '13')

    for(let i = 0; i < count; i+=1) {
        let element = obj_lagelPage.navigateToEachSubMenuOfPrivacyPolicy(i)
        utils.containsText(element, subMenuTitles[i])
        }
});