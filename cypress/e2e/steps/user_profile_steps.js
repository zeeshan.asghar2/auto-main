/* global cy */
/* global Cypress */

import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import utils from '../../utils/utils';
import NavBarUserProfile from '../../po/navbar_user_profile';

let obj_userProfile = new NavBarUserProfile();

Given('User navigates to the user profile', () => {
    obj_userProfile.clickUserProfileIconBtn()
    obj_userProfile.navigateManageProfilePage()
});

Given('User profile page should open', () => {
    utils.verifyIsVisible(obj_userProfile.manageProfileHeader)
});

Given('User input first name', () => {
    obj_userProfile.updateUserProfileValidCase()
});

Given('User clear first name field', () => {
    obj_userProfile.updateUserProfileInValidCase()
});

Given('User clicks on change password link', () => {
    obj_userProfile.clickManageProfileChangePasswordLink()
});

Given('Change password screen should show', () => {
    utils.verifyIsVisible(obj_userProfile.changePasswordPageHeader)
});


When('User click save button of user profile', () => {
    obj_userProfile.clickUserProfileSaveButton()
});

When('User input password as {string}', (password) => {
    obj_userProfile.inputNewPassword(password)
});

Given('User input password as {string} in new password field', (password) => {
    obj_userProfile.inputNewPassword(password)
});

When('User input password as {string} in confirm password field', (confirmPassword) => {
    obj_userProfile.inputConfirmPassword(confirmPassword)
});

When('User clicks on multi factor link', () => {
    obj_userProfile.clickMultiFactorLinkButton()
});

Then('User profile should be update successfully', () => {
    utils.verifyIsVisible(obj_userProfile.userProfileUpdateSuccess)
});

Then('User profile success message should show', () => {
    utils.containsText(obj_userProfile.userProfileUpdateSuccess, "Profile details updated successfully!")
});

Then('User profile should not be update successfully', () => {
    utils.verifyIsVisible(obj_userProfile.emptyNameFieldErrorMessage)
});

Then('User profile error message should show', () => {
    utils.containsText(obj_userProfile.emptyNameFieldErrorMessage, "Name too short.")
});

Then('User should get an error message {string}', (errorMessage) => {
    utils.verifyIsVisible(obj_userProfile.passwordInputError)
    utils.containsText(obj_userProfile.passwordInputError, errorMessage)
});

Then('User should get an error message {string} for confirm password', (errorMessage) => {
    utils.verifyIsVisible(obj_userProfile.confirmPasswordInputError)
    utils.containsText(obj_userProfile.confirmPasswordInputError, errorMessage)
});

Then('Multi factor authentication page should show', () => {
    utils.verifyIsVisible(obj_userProfile.multiFactorHeader)
    utils.containsText(obj_userProfile.multiFactorHeader, "Multi-Factor Authentication")
});