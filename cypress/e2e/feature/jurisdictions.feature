Feature: Jurisdictions functionality Verification

    Background:
        Given User is on home page of srb
        And home page loaded successfully

    Scenario: UK Jurisdictions regulations verification
        When User clicks on UK jurisdiction
        Then All the regulations associated with UK jurisdiction should show correctly

    Scenario: US Jurisdictions regulations verification
        When User clicks on US jurisdiction
        Then All the regulations associated with US jurisdiction should show correctly

    Scenario: EU Jurisdictions regulations verification
        When User clicks on EU jurisdiction
        Then All the regulations associated with EU jurisdiction should show correctly

    Scenario: Exchanges Jurisdictions regulations verification
        When User clicks on Exchanges jurisdiction
        Then All the regulations associated with Exchanges jurisdiction should show correctly