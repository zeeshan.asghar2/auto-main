Feature: SRB Annotation functionality verification

    Background:
    Given User is on home page of srb
    And home page loaded successfully
    And User navigates to the article
    And Respective article should be loaded successfully

    Scenario: Navigate to annotations page
        When User navigates to any article
        Then Article should be loaded successfully
        And Annotation icon should show

    Scenario: Create annotation successfully
        And User clicks annotation icon
        And User clicks create new button
        And User select or create a new heading of the annotation
        And User input description
        When User click save button
        Then Annotation should be added successfully
        And Annotation created success message should show

    Scenario: Update annotation successfully
        And User clicks purple annotation icon
        And Annotation heading populated
        And User clicks the description box of the created annotation
        And User updates any data of the annotation
        When User click save button
        Then Annotation should be updated successfully
        And Annotation updated success message should show


    Scenario: Delete annotation successfully
        And User clicks purple annotation icon
        And Annotation heading populated
        And User clicks the description box of the created annotation
        When User clicks on delete button icon
        Then Annotation should be deleted successfully
        And Annotation deleted success message should show

    Scenario: Create annotation failed case
        And User clicks annotation icon
        And User clicks create new button
        And User input description
        When User click save button
        Then Annotation should not be added
        And Error message should show for missing annotation heading