Feature: SRB dashbaord functionality

  Background:
    Given User is on home page of srb
    And home page loaded successfully

  Scenario: Home page sections verification
    When User verifies home page sections
    Then User should be able to see all sections of home page


  Scenario: Annotations summary page verification
    When User clicks on see more button
    Then User should be navigated to annotations summary page
    And All the column headers names of annotations summary showing properly

  Scenario: Labels summary page verification
    When User clicks on see more button of labels section
    Then User should be navigated to labels summary page
    And All the column headers names of labels summary showing properly


  Scenario: Updates dashbaord page verification
    When User clicks any notification message
    Then Updates main page should open
    And Clicked notification should be showing at the top successfully
    And Filter options should be showing properly on Updates main page

  Scenario: Video Guides section verification
    And User scrolls down to the bottom of the home page
    When User clicks on video guides option
    Then Video guides dialog should open

  Scenario: User Guide section verification
    And User scrolls down to the bottom of the home page
    When User clicks on User Manual
    Then Single rulebook user guide should open