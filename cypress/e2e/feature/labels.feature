Feature: SRB labels functionality verification

    Background:
        Given User is on home page of srb
        And home page loaded successfully
        And User navigates to the article
        And Respective article should be loaded successfully

    Scenario: Navigate to labels page
        Given User is on home page of srb
        And home page loaded successfully
        When User navigates to any article
        Then Article should be loaded successfully
        And Labels icon should show

    Scenario: Create label successfully
        And User clicks label icon
        And User clicks create new button of label
        And User create a new heading of the label
        And User clicks select text link button
        And User selects text
        And User clicks confirm selection button
        When User click save button of label section
        Then Label should be added successfully
        And Label created success message should show


    Scenario: Update label successfully
        And User clicks label icon
        And User clicks on the three dot menu of the created label
        And User clicks edit button
        And User updates any data of the labels
        When User click save button of label section
        Then Label should be updated successfully
        And Label updated success message should show

    
    Scenario: Delete label successfully
        And User clicks label icon
        And User clicks on the three dot menu of the created label
        When User clicks delete button
        Then Label should be deleted successfully
        And Label deleted success message should show

    Scenario: Create label failed case
        And User clicks label icon
        And User clicks create new button of label
        When User click save button of label section
        Then Label should not be added
        And Label created failed message should show