Feature: Legal page functionality verifcation

    Background: 
        Given User is on home page of srb
        And home page loaded successfully
        And User navigates to the legal page
        And Legal page should open 


    Scenario: Verify main menus of legal page
        When User verifies left bar main menu
        Then Legal main menus should be showing

    Scenario: Verify privacy policy sub menus
        When User clicks on privacy policy
        Then All the sub menus of privacy policy should show
