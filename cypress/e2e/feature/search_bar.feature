Feature: SRB search bar functionality

  Background:
    Given User is on home page of srb
    And home page loaded successfully

  Scenario: Verify user is able to retrieve search results
    And User input "mtf" in search bar
    When User clicks search bar icon
    Then User should be able to see the retrieved results


  Scenario: Verify user is not able to retrieve any search result
    And User input "mtf123" in search bar
    When User clicks search bar icon
    Then User should not be able to see the any results