Feature: SRB Login Page

Scenario: Login with valid credentials
  Given User is on Login page of SRB application
  And User input valid credentials
  When User clicks the login button
  Then User should be able to login successfully and home dashboard page should show

Scenario: Login with invalid credentials
  Given User is on Login page of SRB application
  And User input invalid credentials
  When User clicks the login button
  Then Error message should show