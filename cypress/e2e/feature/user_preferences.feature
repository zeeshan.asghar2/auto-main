Feature: User Preferences functionality verifcation

    Background: 
        Given User is on home page of srb
        And home page loaded successfully
        And User navigates to the user preferences
        And User preferences page should open 


    Scenario: Verify User Preferences page shows data table
        When User verifies data table of user preferences
        Then User preferences page should shows all the columns
        And User preferences columns name should be correct

    Scenario: Verify User Preferences page showing all jurisdictions at the top
        When User verifies the jurisdictions are showing on user preferences page
        Then All four jurisdictions should be visible on user preferences page

    Scenario: Navigate to user preferences information dialog
        When User clicks info icon button
        Then User preferences information dialog screen should open
        And User should be able to see close button
        And User should be able to click close button
