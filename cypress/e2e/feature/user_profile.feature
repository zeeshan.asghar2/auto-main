Feature: User Profile functionality verifcation

    Background: 
        Given User is on home page of srb
        And home page loaded successfully
        And User navigates to the user profile
        And User profile page should open 


    Scenario: Update user profile successfully
        And User input first name
        When User click save button of user profile
        Then User profile should be update successfully
        And User profile success message should show

    Scenario: Update user profile failed case
        And User clear first name field
        When User click save button of user profile
        Then User profile should not be update successfully
        And User profile error message should show

    Scenario Outline: Change password by using invalid combination of characters
        And User clicks on change password link
        And Change password screen should show
        When User input password as "<password>"
        Then User should get an error message "<errorMessage>"

        Examples:
            | password                   | errorMessage                                  |
            | 1234@Kaizen                | Password must be at least 14 characters long. |
            | 1999@KAIZENREPORTING       | Password must contain a lowercase character.  |
            | 1999@kaizenreporting       | Password must contain an uppercase character. |
            | NinetyNine@KaizenReporting | Password must contain a number.               |
            | 1999KaizenReporting        | Password must contain a special character.    |

    Scenario: Change password must match case
        And User clicks on change password link
        And Change password screen should show
        And User input password as "SingleRuleBook@12345"
        When User input password as "SingleRuleBook@123456" in confirm password field
        Then User should get an error message "Passwords must match" for confirm password

    Scenario: Navigate to multi factor authentication page
        When User clicks on multi factor link
        Then Multi factor authentication page should show
