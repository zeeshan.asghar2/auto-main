/* global Cypress */
/* global cy */

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:s
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import SRBLoginPage from "../po/srb_login";

let obj_login = new SRBLoginPage()


Cypress.Commands.overwrite("log", (subject, message) => cy.task("log", message))

Cypress.Commands.add('loginSRB', (username, password) =>{

    cy.visit(Cypress.config("baseUrl"))
    cy.get(obj_login.userNameField)
    .type(username)
    .get(obj_login.passwordField)
    .type(password)
    .get(obj_login.loginButton)
        .click({ force: true })

    cy.url().should('include', '/srb')
})
