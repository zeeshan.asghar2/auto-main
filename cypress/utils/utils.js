/* global cy */


//require("cross-fetch/polyfill")
import 'cross-fetch/polyfill'
//const AmazonCognitoIdentity = require('amazon-cognito-identity-js')
import * as AmazonCognitoIdentity from 'amazon-cognito-identity-js'

const users = {
  KaizenAdmin: {
    username: "portal.admin@kaizenreporting.com",
    password: "KaizenPassword123!",
    user_pool_id: "eu-west-1_tZqdDHoRu",
    app_client_id: "4sbsmac10n0a04mg6o3kt19e9q",
  },
  KaizenStandard: {
    username: "portal.standard@kaizenreporting.com",
    password: "KaizenPassword123!",
    user_pool_id: "eu-west-1_tZqdDHoRu",
    app_client_id: "4sbsmac10n0a04mg6o3kt19e9q",
  },
  Client1Admin: {
    username: "portal.admin@kaizenreporting.com",
    password: "Client1Password123!",
    user_pool_id: "eu-west-1_SIcTwVSnw",
    app_client_id: "6oc7mlnhromijqn3adfjpmeu7k",
  },
  Thamu: {
    username: "thamu.gurung@kaizenreporting.com",
    password: "1296@helloKaizen",
    user_pool_id: "eu-west-1_5CmJIfyqc",
    app_client_id: "f703bae2j8l22mhai27sgec37",
  },
  Client1Standard: {
    username: "portal.standard@kaizenreporting.com",
    password: "Client1Password123!",
    user_pool_id: "eu-west-1_SIcTwVSnw",
    app_client_id: "6oc7mlnhromijqn3adfjpmeu7k",
  },
  Client1AdminEntity1: {
    username: "portal.admin.entity1@kaizenreporting.com",
    password: "Client1E1Password123!",
    user_pool_id: "eu-west-1_SIcTwVSnw",
    app_client_id: "6oc7mlnhromijqn3adfjpmeu7k",
  },
  Client1StandardEntity1: {
    username: "portal.standard.entity1@kaizenreporting.com",
    password: "Client1E1Password123!",
    user_pool_id: "eu-west-1_SIcTwVSnw",
    app_client_id: "6oc7mlnhromijqn3adfjpmeu7k",
  },
  Client1AdminEntity2: {
    username: "portal.admin.entity2@kaizenreporting.com",
    password: "Client1E2Password123!",
    user_pool_id: "eu-west-1_SIcTwVSnw",
    app_client_id: "6oc7mlnhromijqn3adfjpmeu7k",
  },
  Client1StandardEntity2: {
    username: "portal.standard.entity2@kaizenreporting.com",
    password: "Client1E2Password123!",
    user_pool_id: "eu-west-1_SIcTwVSnw",
    app_client_id: "6oc7mlnhromijqn3adfjpmeu7k",
  },
  Client2Admin: {
    username: "portal.admin@kaizenreporting.com",
    password: "Client2Password123!",
    user_pool_id: "eu-west-1_7I5RLgjtF",
    app_client_id: "64207qctl8f5e9iphig8tpvn07",
  },
  Client2Standard: {
    username: "portal.standard@kaizenreporting.com",
    password: "Client2Password123!",
    user_pool_id: "eu-west-1_7I5RLgjtF",
    app_client_id: "64207qctl8f5e9iphig8tpvn07",
  },
}

export default {
  getJWTToken: (user) =>
    new Promise((resolve, reject) => {
      const authenticationData = {
        Username: users[user].username,
        Password: users[user].password,
      }
      const authenticationDetails =
        new AmazonCognitoIdentity.AuthenticationDetails(authenticationData)
      const poolData = {
        UserPoolId: users[user].user_pool_id,
        ClientId: users[user].app_client_id,
      }
      const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData)
      const userData = {
        Username: users[user].username,
        Pool: userPool,
      }

      const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData)

      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess(result) {
          const idToken = result.idToken.jwtToken
          resolve(`Bearer ${idToken}`)
        },
        onFailure(err) {
          reject(err)
          // console.log(err)
        },
      })
    }),
  makeRequest: (method, url, token, body) => {
    cy.request({
      method,
      url,
      headers: {
        "Content-Type": "application/json",
        authorization: token,
      },
      body,
    }).as("resp")
  },
  interceptRequest: (method, url, fixture) => {
    cy.intercept(method, url, { fixture })
  },
  verifyIsVisible: (element) => {
    cy.get(element, { timeout: 30000 }).should("be.visible")
  },
  verifyIsNotVisible: (element) => {
    cy.get(element, { timeout: 30000 }).should("not.be.visible")
  },
  clickOnElementIndex(elementSelector, index) {
    cy.get(elementSelector, { timeout: 30000 }).eq(index).click({ force: true })
  },
  clickOnElement(elementSelector) {
    cy.get(elementSelector, { timeout: 40000 }).click({ force: true })
  },
  verifyText(elementSelector, text) {
    cy.get(elementSelector, { timeout: 30000 }).should("have.text", text)
  },

  containsText(elementSelector, text) {
    cy.get(elementSelector, { timeout: 40000 }).should("contain", text)
  },

  verifyElementIndexText(elementSelector, index, text) {
    cy.get(elementSelector, { timeout: 30000 })
      .eq(index)
      .should("have.text", text)
  },

  verifyElementTestText(elementSelector, text) {
    cy.get(elementSelector, { timeout: 30000 })
      .should("have.text", text)
  },

  typeText(elementSelector, text) {
    cy.get(elementSelector, { timeout: 30000 }).type(text)
  },
  scrollToBottom(elementSelector) {
    cy.get(elementSelector).scrollTo("bottom")
  },
  verifyElementsLength(elementSelector, length) {
    cy.get(elementSelector, { timeout: 30000 }).should("have.length", length)
  },
}
